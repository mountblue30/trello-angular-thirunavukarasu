import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoardInfoComponent } from './components/board-info/board-info.component';
import { BoardComponent } from './components/board/board.component';

import { BoardsComponent } from './components/boards/boards.component';

const routes: Routes = [
  {path:"boards",component:BoardsComponent},
  {path:"",redirectTo:"/boards",pathMatch:"full"},
  {path:"boards/:id",component:BoardInfoComponent},
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  BoardsComponent,
  BoardComponent,
  BoardInfoComponent
]
