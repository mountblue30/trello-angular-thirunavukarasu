import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';

import { AppRoutingModule,routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
// import { BoardsComponent } from './components/boards/boards.component';


import { BoardsService } from './services/boards.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BoardInfoComponent } from './components/board-info/board-info.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { CardComponent } from './components/card/card.component';
import { CardModalComponent } from './components/card-modal/card-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    routingComponents,
    BoardInfoComponent,
    ListItemComponent,
    CardComponent,
    CardModalComponent
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatMenuModule,
    MatDialogModule,
  ],
  providers: [BoardsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
