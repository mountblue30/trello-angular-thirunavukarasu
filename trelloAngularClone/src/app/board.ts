export interface Board {
    id:string,
    name: string,
    desc: string,
    prefs:{backgroundColor:string,backgroundImage:string,backgroundImageScaled:[]}
}

export interface List {
    id: string,
    name: string,
}

export interface Card {
    id: string,
    name: string,
}

export interface CheckList {
    id: string,
    checkItems:[
        
    ]
}

export interface CardDetails {
    id:string,
    name:string,
    checkItems: [

    ]
}

export interface CheckItem {
    id:string,
    name:string,
    idChecklist: string,
    state: string,
}