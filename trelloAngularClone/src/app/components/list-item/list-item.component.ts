import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { Card, List } from 'src/app/board';
import { CardsService } from 'src/app/services/cards.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input() list!:List;
  @Output() notifyParent: EventEmitter<Card> = new EventEmitter();
  
  public showToast = false;
  public cards: Card[] = [];

  constructor(private cardService: CardsService){

  }
  ngOnInit() {
    this.getCards()
  }
  archiveList(list:List): void {
    this.notifyParent.emit(list);
    this.showToast = true;
  }
  getCards(): void{
    this.cardService.getAllCards(this.list.id).subscribe((cards)=>{
      this.cards = cards;
    })
  }
  getCardName(name:string,list:List){
    this.cardService.addCard(name,list.id).subscribe((card)=>{
      this.cards.push(card)
    })
  }
  deleteCard(deleteCard: Card) {
    this.cardService.deleteCard(deleteCard.id).subscribe(()=>{
      this.cards = this.cards.filter((card)=>{
        return card.id !== deleteCard.id;
      })
    })
  }
}
