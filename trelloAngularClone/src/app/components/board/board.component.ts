import { Component,Input, OnInit } from '@angular/core';
import { Board } from 'src/app/board';
@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  @Input() board!: Board;
  styleObject(): Object {
       if (this.board.prefs.backgroundColor){
           return {'background-color': this.board.prefs.backgroundColor}
       }
       return {'background': `url(${this.board.prefs.backgroundImage})`, 'background-size' : 'cover'}
   }
  ngOnInit() {
    
    
  }
}
