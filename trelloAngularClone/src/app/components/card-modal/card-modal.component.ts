import { Component,Inject } from '@angular/core';
import { Card, CheckList } from 'src/app/board';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChecklistsService } from 'src/app/services/checklists.service';
import { CheckitemService } from 'src/app/services/checkitem.service';
@Component({
  selector: 'app-card-modal',
  templateUrl: './card-modal.component.html',
  styleUrls: ['./card-modal.component.css']
})
export class CardModalComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data:any,private checklistService: ChecklistsService,private checkItemService: CheckitemService){

  }
  ngOnInit(){
    console.log(this.data.contents);
    
  }
  createCheckList(card:Card,name:string){
    this.checklistService.addChecklist(card.id,name).subscribe((data)=>{
      if (this.data.contents !== undefined){
        this.data.contents.push(data);
      } else {
        this.data.contents = [data]
      }
    })
  }
  deleteCheckList(deleteChecklist:CheckList){
    this.checklistService.deleteChecklist(deleteChecklist.id).subscribe(()=>{
      this.data.contents = this.data.contents.filter((content:any)=>{
        return content.id !== deleteChecklist.id
      }) 
    })
  }

  createCheckItem(checklistId: string,name:string){
    this.checkItemService.addCheckItem(checklistId,name).subscribe((data)=>{
      this.data.contents = this.data.contents.map((content:any)=>{
        if(content.id === checklistId){
          content.checkItems.push(data)
        }
        return content;
      })
    })
  }
  deleteCheckItem(checklistId:string,checkitemId:string){
    this.checkItemService.deleteCheckItem(checklistId,checkitemId).subscribe(()=>{
      
      })
  }
}
