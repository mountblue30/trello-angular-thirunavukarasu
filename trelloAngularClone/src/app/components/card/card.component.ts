import { Component, Input, OnInit,EventEmitter,Output } from '@angular/core';
import { Card, CheckList,CardDetails } from 'src/app/board';
import { ChecklistsService } from 'src/app/services/checklists.service';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CardModalComponent } from '../card-modal/card-modal.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() card!: Card;
  @Output() onDeleteCard: EventEmitter<Card> = new EventEmitter();
  
  public checklistItems:CheckList[] = []
  public cardDetails: CardDetails = { id: "", name: "", checkItems: [] }
  // if(card:Card){
  //   this.cardDetails["name"] = card.name
  // }
  // public modalName: string = ""
  constructor(private checklistService: ChecklistsService,public dialog: MatDialog){
    
  }
  ngOnInit(): void {


  }
  getAllChecklists(card: Card){
    this.checklistService.getAllCheckLists(card.id).subscribe((data)=>{
      console.log(data);
      
    })
  }
  deleteCard(card:Card){
    this.onDeleteCard.emit(card);
  }
  // getcardDetails(card:Card){
  //   this.checklistService.getAllCheckLists(card.id).subscribe((data)=>{
  //     console.log(data);
      
  //     if (data.length !== 0){
  //       this.checklistItems = data[0].checkItems
  //       this.cardDetails["checkItems"] = data[0].checkItems
  //       this.cardDetails["id"] = card.id;
  //       this.cardDetails["name"] = card.name;
  //       this.modalName = card.name
  //     } else {
  //       console.log(1);
        
  //       this.modalName = card.name
  //     }
  //   })    
  // }
  openDialog(card:Card){
    this.checklistService.getAllCheckLists(card.id).subscribe((items)=>{
      if (items.length > 0) {
        this.dialog.open(CardModalComponent, {
          data: {
            card, contents: items
          }
        })
        
      }else {
        this.dialog.open(CardModalComponent, {
          data: {
            card
          }
        })
      }
      
      
    })
    
    
  }
}
