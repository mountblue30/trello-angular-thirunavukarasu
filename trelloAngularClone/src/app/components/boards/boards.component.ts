import { Component, OnInit } from '@angular/core';
import { faL, faTimes } from '@fortawesome/free-solid-svg-icons'
import { BoardsService } from 'src/app/services/boards.service';
import { Board } from 'src/app/board';
@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css']
})
export class BoardsComponent implements OnInit {
  public boards: Board[] = [];
  public popupClass: string = "popup";
  public value: string = 'Clear me';
  public faTimes = faTimes;
  public showToast: boolean = false;
  constructor(private _boardsService: BoardsService) {

  }
  ngOnInit() {
    this._boardsService.getBoards().subscribe((boards) => {
      this.boards = boards;
    });

  }
  togglePopup(): void {
    if (this.popupClass === "popup") {
      this.popupClass = "popup show"
    } else {
      this.popupClass = "popup"
    }
  }
  getBoardName(name: string) {
    if (name != "") {
      this._boardsService.createBoard({ name } as Board).subscribe((board) => {
        this.boards.unshift(board)
        this.showToast = true;
      });
    }
  }
}
