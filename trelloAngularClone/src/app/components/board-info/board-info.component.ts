import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Card, List } from 'src/app/board';
import { ListsService } from 'src/app/services/lists.service';
@Component({
  selector: 'app-board-info',
  templateUrl: './board-info.component.html',
  styleUrls: ['./board-info.component.css']
})
export class BoardInfoComponent implements OnInit {
  public id:string = "";
  public lists: List[] = [];
  public input:string = "";
  public showToast = false
  constructor(private route:ActivatedRoute,private listService: ListsService){

  }
  ngOnInit(){
    let id = this.route.snapshot.params["id"];
    this.listService.getAllLists(id).subscribe((lists)=>{
      this.lists = lists;
    });
    
  }
  getListName(name: string) {
    let id = this.route.snapshot.params["id"];
    if (name != "") {
      this.listService.createList({ name } as List,id as string).subscribe((list) => {
        this.lists.unshift(list);
        this.input = "";
        this.showToast = true;
      });
    }
  }
  archiveList(deleteList: List) {
    this.listService.archiveList(deleteList.id).subscribe(() => {
      this.lists = this.lists.filter((list) => {
        return list.id !== deleteList.id;
      })
    })
  }
  
}
