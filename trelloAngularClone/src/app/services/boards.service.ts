import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Board } from '../board';
@Injectable({
  providedIn: 'root'
})
export class BoardsService {
  private apiUrl: string = "https://api.trello.com/"
  private key:string = "917d1e0c6c848585519f8146c2791990";
  private token: string= "1f2133c3d95ad016327fff699029569ddb202dc5dac4a4507e1d39b74f98d107"
  constructor(private http: HttpClient) { }
  getBoards(): Observable<Board[]> {
    return this.http.get<Board[]>(`${this.apiUrl}1/members/me/boards?key=${this.key}&token=${this.token}`)
  }
  createBoard(name: Board):Observable<Board>{
    alert(name)
    return this.http.post<Board>(`https://api.trello.com/1/boards?name=${name}&key=${this.key}&token=${this.token}`,name)
  }
}
