import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CheckItem } from '../board';

@Injectable({
  providedIn: 'root'
})
export class CheckitemService {
  private apiUrl: string = "https://api.trello.com/"
  private key: string = "917d1e0c6c848585519f8146c2791990";
  private token: string = "1f2133c3d95ad016327fff699029569ddb202dc5dac4a4507e1d39b74f98d107"
  constructor(private http: HttpClient) { }


  // getAllCheckItems(cardId: string): Observable<CheckList[]> {
  //   return this.http.get<CheckList[]>(`${this.apiUrl}1/cards/${cardId}/checklists?key=${this.key}&token=${this.token}`);
  // }
  addCheckItem(checkListId: string, name: string): Observable<CheckItem> {
    return this.http.post<CheckItem>(`${this.apiUrl}/1/checklists/${checkListId}/checkItems?key=${this.key}&token=${this.token}&name=${name}`, { name })
  }
  deleteCheckItem(checklistId: string, checkitemId:string): Observable<CheckItem> {
    return this.http.delete<CheckItem>(`${this.apiUrl}1/checklists/${checklistId}/checkItems/${checkitemId}?key=${this.key}&token=${this.token}`)
  }
}
