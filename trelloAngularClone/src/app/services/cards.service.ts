import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Board, Card } from '../board';
@Injectable({
  providedIn: 'root'
})
export class CardsService {
  private apiUrl: string = "https://api.trello.com/"
  private key: string = "917d1e0c6c848585519f8146c2791990";
  private token: string = "1f2133c3d95ad016327fff699029569ddb202dc5dac4a4507e1d39b74f98d107"
  constructor(private http: HttpClient) { }

  getAllCards(listId:string):Observable<Card[]>{
    return this.http.get<Card[]>(`${this.apiUrl}1/lists/${listId}/cards?key=${this.key}&token=${this.token}`);
  }
  addCard(name:string,listId:string):Observable<Card>{
    return this.http.post<Card>(`${this.apiUrl}1/cards?idList=${listId}&key=${this.key}&token=${this.token}&name=${name}`,{name})
  }
  deleteCard(cardId: string):Observable<Card> { 
    return this.http.delete<Card>(`${this.apiUrl}1/cards/${cardId}?key=${this.key}&token=${this.token}`)
  }
}
