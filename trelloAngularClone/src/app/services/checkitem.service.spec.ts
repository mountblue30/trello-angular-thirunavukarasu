import { TestBed } from '@angular/core/testing';

import { CheckitemService } from './checkitem.service';

describe('CheckitemService', () => {
  let service: CheckitemService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CheckitemService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
