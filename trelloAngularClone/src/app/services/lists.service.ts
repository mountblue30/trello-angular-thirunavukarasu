import { Injectable } from '@angular/core';
import { List } from '../board';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ListsService {
  private apiUrl: string = "https://api.trello.com/"
  private key:string = "917d1e0c6c848585519f8146c2791990";
  private token: string= "1f2133c3d95ad016327fff699029569ddb202dc5dac4a4507e1d39b74f98d107"
  constructor(private http: HttpClient) { }

  getAllLists(boardId:string): Observable<List[]> {
    return this.http.get<List[]>(`${this.apiUrl}1/boards/${boardId}/lists?key=${this.key}&token=${this.token}`);
  }
  createList(name: List, boardId: string): Observable<List> {
    return this.http.post<List>(`${this.apiUrl}1/boards/${boardId}/lists?name=${name}&key=${this.key}&token=${this.token}`, name)
  }
  archiveList(id:string){
    return this.http.put<List>(`https://api.trello.com/1/lists/${id}/closed?key=${this.key}&token=${this.token}&value=true`,{value:true});
  }
}
