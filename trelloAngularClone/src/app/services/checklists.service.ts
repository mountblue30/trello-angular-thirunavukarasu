import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CheckList } from '../board';
@Injectable({
  providedIn: 'root'
})
export class ChecklistsService {

  private apiUrl: string = "https://api.trello.com/"
  private key: string = "917d1e0c6c848585519f8146c2791990";
  private token: string = "1f2133c3d95ad016327fff699029569ddb202dc5dac4a4507e1d39b74f98d107"
  constructor(private http: HttpClient) { }


  getAllCheckLists(cardId: string): Observable<CheckList[]> {
    return this.http.get<CheckList[]>(`${this.apiUrl}1/cards/${cardId}/checklists?key=${this.key}&token=${this.token}`);
  }
  addChecklist(cardId: string,name:string): Observable<CheckList> {
    return this.http.post<CheckList>(`${this.apiUrl}1/checklists?idCard=${cardId}&key=${this.key}&token=${this.token}&name=${name}`, { name })
  }
  deleteChecklist(checklistId: string): Observable<CheckList> {
    return this.http.delete<CheckList>(`${this.apiUrl}1/checklists/${checklistId}?key=${this.key}&token=${this.token}`)
  }
}
